@extends('layouts.admin-dashboard')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Banner {{ (isset($banner) && $banner->count()) > 0 ? 'Update' : 'Add' }} </h1>
    <div class="row">

    <div class="col-12">
            @if(isset($banner) )
            {{ Form::open(['url'=>route('banner.update', $banner->id), 'class'=>'form', 'enctype'=>'multipart/form-data']) }}
            @method('PUT')
            @else
            {{ Form::open(['url' =>route('banner.store'), 'class'=>'Form', 'enctype' =>'multipart/form-data']) }}
            @endif
            <div class="form-group row">
                {{ Form::label('title', 'Title: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::text('title', @$banner->title, ['class'=>'form-control form-control-sm', 'id'=>'title']) }}
                    @if($errors->has('title'))
                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('link', 'Link: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::url('link', @$banner->link, ['class'=>'form-control form-control-sm', 'id'=>'url']) }}
                    @if($errors->has('url'))
                    <span class="alert-danger">{{ $errors->first('url') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('status', 'Status: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::select('status',['active'=> 'Active','inactive'=>'Inactive'], @$banner->status, ['class'=>'form-control form-control-sm', 'id'=>'status', 'required'=>true]) }}
                    @if($errors->has('status'))
                    <span class="alert-danger">{{ $errors->first('status') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('image', 'Image: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-4">
                    {{ Form::file('image', ['id'=>'image', 'required'=> (isset($banner)) ? false : true, 'accept'=>'image/*']) }}
                    @if($errors->has('image'))
                    <span class="alert-danger">{{ $errors->first('image') }}</span>
                    @endif
                </div>

                @if(isset($banner) && file_exists(public_path().'/uploads/banners/'.$banner->image))
                <div class="col-sm-4">
                    <img src="{{ asset('uploads/banners/'.$banner->image) }}" alt="" class="img img-responsive img-thumbnail">
                </div>
                @endif
            </div>

            <div class="form-group row">
                {{ Form::label('submit','', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::button('<i class="fas fa-paper-plane"></i> Submit', ['id'=>'submit', 'class'=> 'btn btn-success', 'type'=>'submit']) }}
                    {{ Form::button('<i class="fas fa-trash"></i> Cancel', ['id'=>'cancel', 'class'=> 'btn btn-danger', 'type'=>'reset']) }}
                </div>
            </div>


        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@section('scripts')
<script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
<script>
    ClassicEditor.create(document.querySelector('#description'));
    $('#datepicker').datepicker();
</script>
@endsection