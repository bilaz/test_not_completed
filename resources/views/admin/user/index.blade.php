@extends('layouts.admin-dashboard')

@section('title')
User List || Admin, EduTraining
@endsection
@section('datataleslinks')
<link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.dataTables.min.css') }}">
@endsection
@section('datatablesscripts')
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script>
    $('.table').dataTable();
</script>
@endsection


@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">User List
    </h1>
    @include('admin.section.notification')
    <table class="table">
        <thead>
            <th>S.N </th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
        @if($user_data)
            @foreach($user_data as $key=>$value)
            <tr>
                <td> {{ $key+1 }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ ucfirst($value->role)}}</td>
                <td>{{ ucfirst($value->status) }}</td>

                <td> <a href="{{ route('user.edit', $value->id) }}" style="border-radius: 50%" class="btn btn-success">
                        <i class="fas fa-pencil-alt"></i>
                    </a>

                    <form onsubmit="return confirm('Are you sure you want to delete this user?')" action="{{ route('user.destroy', $value->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" style="border-radius: 50%">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

</div>
@endsection