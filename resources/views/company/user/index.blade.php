@extends('layouts.company-dashboard')

@section('title')
User List || user, EduTraining
@endsection
@section('datataleslinks')
<link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.dataTables.min.css') }}">
@endsection
@section('datatablesscripts')
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script>
    $('.table').dataTable();
</script>
@endsection


@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">User List
    </h1>
    @include('company.section.notification')
    <table class="table">
        <thead>
            <th>S.N </th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
        </thead>
        <tbody>
        @if($user_data)
            @foreach($user_data as $key=>$value)
            <tr>
                <td> {{ $key+1 }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ ucfirst($value->role)}}</td>
                <td>{{ ucfirst($value->status) }}</td>

            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

</div>
@endsection