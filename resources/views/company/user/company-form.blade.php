@extends('layouts.company-dashboard')

@section('scripts')
<script>
    $('#change_password').change(function() {
        var checked = $('#change_password').prop('checked');
        if (checked) {
            $('#password').attr('required');
            $('#password_confirm').attr('required');
            $('#change_password_div').removeClass('d-none');
        } else {
            $('#password').removeAttr('required');
            $('#password_confirm').removeAttr('required');
            $('#change_password_div').addClass('d-none');
        }
    });
</script>
@endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Company {{ isset($user_data) ? 'update' : 'add'}} </h1>
    <div class="row">

        <div class="col-12">
            {{Form::open(['url'=>route('company-update',$user_data->id),'class'=>'form', 'enctype'=>'multipart/form-data'])  }}
            @method('PUT')




            <div class="form-group row">
                {{ Form::label('name', 'Name: ',['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{Form::text('name', @$user_data->name ,['class'=>'form-control','id'=>'name']) }}
                    @if($errors->has('name'))
                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>

            

            <div class="form-group row">
                {{ Form::label('change_password', 'Change password (optional):  ',['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{Form::checkbox('change_password',1,false,['id'=>'change_password']) }} Yes
                    @if($errors->has('change_password'))
                    <span class="alert-danger">{{ $errors->first('change_password') }}</span>
                    @endif
                </div>
            </div>


            <div class="d-none" id="change_password_div">

                <div class="form-group row">
                    {{ Form::label('password', 'Password: ',['class'=>'col-sm-3']) }}
                    <div class="col-sm-9">
                        {{Form::password('password',['id'=>'password','class'=>'form-control']) }}
                        @if($errors->has('password'))
                        <span class="alert-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{ Form::label('confirm_password', 'Reenter password: ',['class'=>'col-sm-3']) }}
                    <div class="col-sm-9">
                        {{Form::password('password_confirmation',['id'=>'confirm_password','class'=>'form-control']) }}
                        @if($errors->has('confirm_password'))
                        <span class="alert-danger">{{ $errors->first('confirm_password') }}</span>
                        @endif
                    </div>
                </div>

            </div>


            <div class="form-group row">
                {{ Form::label('', '',['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{Form::button('<i class ="fas fa-plane"></i> Submit',['class'=>'btn btn-success','id'=>'submit','type'=>'submit']) }}
                    {{Form::button('<i class ="fas fa-trash"></i> Cancel',['class'=>'btn btn-danger','id'=>'cancel','type'=>'reset']) }}

                </div>
            </div>


            {{Form::close()}}

        </div>
        @endsection