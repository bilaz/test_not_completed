@extends('layouts.company-dashboard')

@section('title')
Employee List || Company, Company
@endsection
@section('datataleslinks')
<link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.dataTables.min.css') }}">
@endsection
@section('datatablesscripts')
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script>
    $('.table').dataTable();
</script>
@endsection


@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Employee List
        <a href="{{ route('employee.create') }}" class="btn btn-success float-right">
            <i class="fas fa-fw fa-plus"></i>
            Add Employee
        </a>
    </h1>
    @include('admin.section.notification')
    <table class="table">
        <thead>
            <th>S.N </th>
            <th>Name</th>
            <th>Image</th>
            <th>Role</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
        @if($employee_data)
            @foreach($employee_data as $key=>$value)
            <tr>
                <td> {{ $key+1 }}</td>
                <td>{{ $value->name}}</td>
                <td>
                    @if($value->image !=null && file_exists(public_path().'/uploads/employees/'.$value->image))
                    <img style="max-width: 150px;" src="{{ asset('uploads/employees/'.$value->image) }}" alt="" class="img img-responsive img-thumbnail">
                    @endif
                </td>
                <td>{{ ucfirst($value->role) }}</td>
                <td>{{ ucfirst($value->status) }}</td>

                <td> <a href="{{ route('employee.edit', $value->id) }}" style="border-radius: 50%" class="btn btn-success">
                        <i class="fas fa-pencil-alt"></i>
                    </a>

                    <form onsubmit="return confirm('Are you sure you want to delete this employee?')" action="{{ route('employee.destroy', $value->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" style="border-radius: 50%">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
        </tbody>
    </table>

</div>
        

@endsection