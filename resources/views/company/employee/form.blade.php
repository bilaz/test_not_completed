@extends('layouts.company-dashboard')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">employee {{ (isset($employee) && $employee->count()) > 0 ? 'Update' : 'Add' }} </h1>
    <div class="row">

        <div class="col-12">
            @if(isset($employee) )
            {{ Form::open(['url'=>route('employee.update', $employee->id), 'class'=>'form', 'enctype'=>'multipart/form-data']) }}
            @method('PUT')
            @else
            {{ Form::open(['url' =>route('employee.store'), 'class'=>'Form', 'enctype' =>'multipart/form-data']) }}
            @endif
            <div class="form-group row">
                {{ Form::label('name', 'Name: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::text('name', @$employee->name, ['class'=>'form-control form-control-sm', 'id'=>'name']) }}
                    @if($errors->has('name'))
                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('skills','Employee: ',['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{Form::textarea('skills', @$employee->skills,['class'=>'form-control','id'=>'skills']) }}
                    @if($errors->has('skills'))
                    <span class="alert-danger">
                        {{ $errors->first('skills') }}
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('description','Description: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('description',  @$employee->description, ['class'=>'form-control','rows'=>5, 'style'=>'resize:none', 'id'=>'description']) }}
                    @if($errors->has('description'))
                    <span class="alert-danger">{{ $errors->first('description') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('address','Address: ',['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{Form::textarea('address', @$employee->address,['class'=>'form-control','id'=>'address']) }}
                    @if($errors->has('address'))
                    <span class="alert-danger">
                        {{ $errors->first('address') }}
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('role', 'Role: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::select('role',['manager'=> 'Manager','receptionist'=>'Receptionist','operator'=>'Operator'], @$employee->role, ['class'=>'form-control form-control-sm', 'id'=>'role', 'required'=>true]) }}
                    @if($errors->has('role'))
                    <span class="alert-danger">{{ $errors->first('role') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('status', 'Status: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::select('status',['active'=> 'Active','inactive'=>'Inactive'], @$employee->status, ['class'=>'form-control form-control-sm', 'id'=>'status', 'required'=>true]) }}
                    @if($errors->has('status'))
                    <span class="alert-danger">{{ $errors->first('status') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('phone','Phone: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::tel('phone', @$employee->phone, ['class'=>'form-control', 'id'=>'phone']) }}
                    @if($errors->has('phone'))
                    <span class="alert-danger">{{ $errors->first('phone') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('image', 'Image: ', ['class'=>'col-sm-3']) }}
                <div class="col-sm-4">
                    {{ Form::file('image', ['id'=>'image', 'required'=> (isset($employee)) ? false : true, 'accept'=>'image/*']) }}
                    @if($errors->has('image'))
                    <span class="alert-danger">{{ $errors->first('image') }}</span>
                    @endif
                </div>

                @if(isset($employee) && file_exists(public_path().'/uploads/companies/'.$employee->image))
                <div class="col-sm-4">
                    <img src="{{ asset('uploads/companies/'.$employee->image) }}" alt="" class="img img-responsive img-thumbnail">
                </div>
                @endif
            </div>

            <div class="form-group row">
                {{ Form::label('','', ['class'=>'col-sm-3']) }}
                <div class="col-sm-9">
                    {{ Form::button('<i class="fas fa-paper-plane"></i> Submit', ['id'=>'submit', 'class'=> 'btn btn-success', 'type'=>'submit']) }}
                    {{ Form::button('<i class="fas fa-trash"></i> Cancel', ['id'=>'cancel', 'class'=> 'btn btn-danger', 'type'=>'reset']) }}
                </div>
            </div>


        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@section('scripts')
<script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
<script>
    ClassicEditor.create(document.querySelector('#description'));
    $('#datepicker').datepicker();
</script>
@endsection