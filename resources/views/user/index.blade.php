@include('home.section.header')

@include('home.section.menu')

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <center>Welcome {{ Auth::user()->name }}</center>
    </h1>
    @include('admin.section.notification')
    <table class="table">
        <thead>
            <th>Name</th>
            <th>View Profile</th>
            <th>Update Profile</th>
            <th>Logout</th>
        </thead>
        <tbody>
            <tr>
                <td>{{ Auth::user()->name }}</td>
                <td>
                    link
                </td>
                <td>
                    link
                </td>
                <td>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Log Out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </td>
            </tr>
        </tbody>
    </table>

</div>

@include('home.section.footer')

@include('home.section.scripts')