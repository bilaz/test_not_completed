<div class="row isotope-grid">

    @if(isset($companies) && $companies->count() > 0)

        @foreach($companies as $prod_info)
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item {{ $prod_info->cat_id }}">
                <!-- Block2 -->
                <div class="block2">
                    <div class="block2-pic hov-img0">
                        <a href="">
                            <img src="{{ asset('uploads/companies/'.$prod_info->image) }}" alt="IMG-PRODUCT">
                        </a>
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                        <div class="block2-txt-child1 flex-col-l ">
                            <a href="" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                {{ $prod_info->name}}
                            </a>

                            <span class="stext-105 cl3">
                               
                            {{ $prod_info->address}}
                                    
                                
                            </span>

                            <a href="javascript:0;" onclick="addToCart(this)" data-product_id="" data-quantity="1">
                            {{ $prod_info->summary}}
                            {!! $prod_info->description!!}
                            </a>
                        </div>


                    </div>
                </div>
            </div>


            

        @endforeach

    @else
        Company Not found
    @endif
</div>
<h1>Employee list</h1>
<div class="row isotope-grid">

    @if(isset($employees) && $employees->count() > 0)

        @foreach($employees as $prod_info)
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item {{ $prod_info->cat_id }}">
                <!-- Block2 -->
                <div class="block2">
                    <div class="block2-pic hov-img0">
                        <a href="">
                            <img src="{{ asset('uploads/employees/'.$prod_info->image) }}" alt="IMG-PRODUCT">
                        </a>
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                        <div class="block2-txt-child1 flex-col-l ">
                            <a href="" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                {{ $prod_info->name}}
                            </a>

                            <span class="stext-105 cl3">
                               
                            {{ $prod_info->address}}
                                    
                                
                            </span>

                            <a href="javascript:0;" onclick="addToCart(this)" data-product_id="" data-quantity="1">
                            {{ $prod_info->summary}}
                            {!! $prod_info->description!!}
                            </a>
                        </div>


                    </div>
                </div>
            </div>


            

        @endforeach

    @else
        Employee Not found
    @endif
</div>

