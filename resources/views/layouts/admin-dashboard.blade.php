@include('admin.section.header')
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->  
    @include('admin.section.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        @include('admin.section.top-nav')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        @yield('content')
        <!-- Ending page content -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      @include('admin.section.copyright')
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  @include('admin.section.logout')

  <!-- Bootstrap core JavaScript-->
  @include('admin.section.scripts')
  
@yield('scripts')
@include('admin.section.footer')