<?php

namespace App\Http\Controllers;
use App\Models\Employee;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $employee = null;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $this->employee =$this->employee->get();
        return view('company.employee.index')->with('employee_data',$this->employee); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.employee.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= $this->employee->getRules(); //validation//   //$rules = $company->getRules(); from models
       $request->validate($rules);
       //dd($request);

       $data= $request->all();   //all(); receive all data inserted from form
       $data['added_by'] = $request->user()->id; //to check whether data is added by admin or user by id

        //dd($request);
        if($request->image){
            $file_name =  uploadImage($request->image, "employees");    //uploadImage is autoloading function from helpers.php
            if($file_name){
                $data['image'] = $file_name;
            } else {
                $data['image'] =null;
            }
        }
        $this->employee->fill($data);
        $success = $this->employee->save();
        if($success){
            $request->session()->flash('success','employee added successfully.');
        } else {
            $request->session()->flash('error','Problem while adding employee.');
        }
        //dd($data);

        return redirect()->route('employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->employee =$this->employee->find($id); // SELECT * FROM company WHERE id = $id //find function from form
        if(!$this->employee){
            request()->session()->flash('error','employee not found');
            return redirect()->route('employee.index');
        }

        return view('company.employee.form')->with('employee',$this->employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules= $this->employee->getRules(); //validation//   //$rules = $company->getRules(); from models
        $request->validate($rules);
        //dd($request);
 
        $data= $request->all();   //all(); receive all data inserted from form
        $this->employee = $this->employee->find($id);
 
         //dd($request);
         if($request->image){
             $file_name =  uploadImage($request->image, "employees");    //uploadImage is autoloading function from helpers.php
             if($file_name){
                 $data['image'] = $file_name;
             } else {
                 $data['image'] =null;
             }
         }
         $this->employee->fill($data);
         $success = $this->employee->save();
         if($success){
             $request->session()->flash('success','employee updated successfully.');
         } else {
             $request->session()->flash('error','Problem while updating employee.');
         }
         //dd($data);
 
         return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->employee = $this->employee->find($id);   // SELECT * FROM company WHERE id = $id //find function is used to find specic data from form
        if(!$this->employee){
            request()->session()->flash('error','Employees not found');
            return redirect()->route('employee.index');
        }

        $image = $this->employee->image;  //store into $image
        $del = $this->employee->delete();    //delete image from database using delete function 
        if($del){
            // image
            if(!empty($image) && file_exists(public_path().'/uploads/employees/'.$image)){
                unlink(public_path().'/uploads/employees/'.$image);
            }
            request()->session()->flash('success','employee Deleted successfully');
        } else {
            request()->session()->flash('error','Problem while deleting employee');
        }
        return redirect()->route('employee.index');  //return to index page
    }
}
