<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(request()->user()->role == 'admin'){
            return redirect()->route('admin');
        } else if(request()->user()->role == 'company'){
            return redirect()->route('company');
        } else {
            return redirect()->route('user');
        }
       // return view('home');
    }

    public function admin(){
        return view('admin.dashboard.index');
    }

    public function company(){
        return view('layouts.company-dashboard');
    }

    public function user(){
        return view('user.index');
    }
}
