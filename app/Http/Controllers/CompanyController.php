<?php

namespace App\Http\Controllers;
use App\Models\Company;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $company = null;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->company = $this->company->get();
        return view('admin.company.index')->with('company_data', $this->company); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= $this->company->getRules(); //validation//   //$rules = $company->getRules(); from models
       $request->validate($rules);
       //dd($request);

       $data= $request->all();   //all(); receive all data inserted from form
       $data['added_by'] = $request->user()->id; //to check whether data is added by admin or user by id

        //dd($request);
        if($request->image){
            $file_name =  uploadImage($request->image, "companies");    //uploadImage is autoloading function from helpers.php
            if($file_name){
                $data['image'] = $file_name;
            } else {
                $data['image'] =null;
            }
        }
        $this->company->fill($data);
        $success = $this->company->save();
        if($success){
            $request->session()->flash('success','company added successfully.');
        } else {
            $request->session()->flash('error','Problem while adding company.');
        }
        //dd($data);

        return redirect()->route('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $this->company =$this->company->find($id); // SELECT * FROM company WHERE id = $id //find function from form
        if(!$this->company){
            request()->session()->flash('error','Company not found');
            return redirect()->route('company.index');
        }

        return view('admin.company.form')->with('company',$this->company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules= $this->company->getRules(); //validation//   //$rules = $company->getRules(); from models
       $request->validate($rules);
       //dd($request);

       $data= $request->all();   //all(); receive all data inserted from form
       $this->company = $this->company->find($id);

        //dd($request);
        if($request->image){
            $file_name =  uploadImage($request->image, "companies");    //uploadImage is autoloading function from helpers.php
            if($file_name){
                $data['image'] = $file_name;
            } else {
                $data['image'] =null;
            }
        }
        $this->company->fill($data);
        $success = $this->company->save();
        if($success){
            $request->session()->flash('success','company updated successfully.');
        } else {
            $request->session()->flash('error','Problem while updating company.');
        }
        //dd($data);

        return redirect()->route('company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->company = $this->company->find($id);   // SELECT * FROM company WHERE id = $id //find function is used to find specic data from form
        if(!$this->company){
            request()->session()->flash('error','Company not found');
            return redirect()->route('company.index');
        }

        $image = $this->company->image;  //store into $image
        $del = $this->company->delete();    //delete image from database using delete function 
        if($del){
            // image
            if(!empty($image) && file_exists(public_path().'/uploads/companies/'.$image)){
                unlink(public_path().'/uploads/companies/'.$image);
            }
            request()->session()->flash('success','Company Deleted successfully');
        } else {
            request()->session()->flash('error','Problem while deleting company');
        }
        return redirect()->route('company.index');  //return to index page
    }

    public function getSearchResult(Request $request){
        // SELECT * FROM companies WHERE status = 'active' AND (name  LIKE '%'.$request->search.'%' OR summary LIKE '%'.$request->search.'%')
        // %test%
        $this->company = $this->company->where('status','active')
                                        ->where(function($query){
                                            global $request;
                                            return $query->Where(
                                                'name','LIKE','%'.$request->search.'%')
                                                ->orWhere('summary', 'LIKE', '%'.$request->search.'%')
                                                ->orWhere('description', 'LIKE', '%'.$request->search.'%');
                                        })->paginate(24);
        return view('home.company-list')->with('companies',$this->company);
    }

}
