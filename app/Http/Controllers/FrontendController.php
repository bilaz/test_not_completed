<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\User;
use App\Models\Banner;
use App\Models\Employee;

class FrontendController extends Controller
{
    protected $banner = null;
    protected $company = null;
    protected $employee = null;

    public function __construct(Company $company,Banner $banner,Employee $employee){
        $this->banner = $banner;
        $this->company = $company;
        $this->employee = $employee;

    }

    public function index()
    {
        $this->banner = $this->banner->where('status','active')->orderBy('id','DESC')->limit(5)->get();
        $this->company = $this->company->where('status','active')->orderBy('id','DESC')->limit(20)->get();
        $this->employee = $this->employee->where('status','active')->orderBy('id','DESC')->limit(20)->get();
        return view('home.index')
        ->with('banner', $this->banner)
        ->with('companies', $this->company)
        ->with('employees', $this->employee);
    }
}
