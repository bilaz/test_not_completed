<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['added_by','name', 'summary', 'description', 'address', 'role', 'image', 'status'];

    public function getRules()
    {
        return [
            'name' => 'nullable|string',
            'image' => 'sometimes|image|max:5000',
            'summary' => 'nullable|string',
            'address' => 'nullable|string',
            'description' => 'nullable|string',
            'role' => 'required|in:manager,receptionist,operator',
            'status' => 'required|in:active,inactive',
        ];
    }

}
