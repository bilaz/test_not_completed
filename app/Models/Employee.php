<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['added_by','name', 'skills', 'description', 'address','phone', 'role', 'image', 'status'];

    public function getRules()
    {
        return [
            'name' => 'nullable|string',
            'image' => 'sometimes|image|max:5000',
            'skills' => 'nullable|string',
            'address' => 'nullable|string',
            'phone' => 'nullable|string',
            'description' => 'nullable|string',
            'role' => 'required|in:manager,receptionist,operator',
            'status' => 'required|in:active,inactive',
        ];
    }
}
