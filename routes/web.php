<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');
// });

/** Frontend Routes */
Route::get('/', 'FrontendController@index')->name('homepage');
Route::get('/search', 'CompanyController@getSearchResult')->name('search');
Route::get('/help-and-faq',function(){
    return view('home.faq');
})->name('help-faq');

Route::get('/about-us', function(){
    return view('home.about-us');
})->name('about-us');

Route::get('/contact-us', function(){
    return view('home.contact');
})->name('contact-us');

/** Frontend Routes closes */

Auth::routes();
Route::get('/userprofile', 'UserController@compUser')->name('compuser');
Route::get('/userprofileedit', 'UserController@compUserEdit')->name('compuseredit');
Route::get('/userprofiledel', 'UserController@compUserDel')->name('compuserdel');
Route::get('/register',function(){
    return view('home.register');
})->name('register');

Route::post('/register','UserController@registerUser')->name('register-user');
Route::get('/active/{token}','UserController@activateUser')->name('activate-user');

// Route::get('/register',function(){
//     return redirect()->route('login');
// });


/** Backend Routes */
Route::group(['middleware'=>'auth'],  function(){
    Route::group(['prefix'=>'admin', 'middleware' => 'admin'], function(){
        Route::get('/', 'HomeController@admin')->name('admin');
        Route::resource('company' ,'CompanyController');
        Route::resource('user' ,'UserController');
        Route::resource('banner' ,'BannerController');

        Route::get('/profile','UserController@adminProfile')->name('admin-profile');
        Route::put('/profile/{id}', 'UserController@updateAdmin')->name('admin-update');
        

    });


    Route::group(['prefix'=>'company', 'middleware' => 'company'], function(){
        Route::get('/', 'HomeController@company')->name('company');
        Route::resource('employee' ,'EmployeeController');
    });


    Route::group(['prefix'=>'user', 'middleware' => 'user'], function(){
        Route::get('/', 'HomeController@user')->name('user');
        Route::resource('profileuser' ,'UserFrontController');
    });

});

Route::get('/home', 'HomeController@index')->name('home');
