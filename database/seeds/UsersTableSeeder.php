<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = array(
            array(
                'name' => 'Admin',
                'email' => 'admin@company.loc',
                'password' => Hash::make('admin234'),
                'role' => 'admin',
                'status' => 'active'
            ),
            array(
                'name' => 'Company',
                'email' => 'company@company.loc',
                'password' => Hash::make('company123'),
                'role' => 'company',
                'status' => 'active'
            ),
            array(
                'name' => 'User',
                'email' => 'user@company.loc',
                'password' => Hash::make('user123'),
                'role' => 'user',
                'status' => 'active'
            )
        );

        DB::table('users')->insert($array);
    }
}
